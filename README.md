### A que se destina este repositório? ###
Corresponder os requisitos do Desafio Luiza Labs

### Configuração do projeto ###
Para executar o projeto basta, após clonar o repositório, executar a instalação das bibliotecas requeridas com o `pip install -r requirements.txt` enquanto no diretório raiz.

Para executar o aplicativo, basta executar o script fb_rest.py. Ex: `python src/fb_rest.py`. A aplicação ficará disponível no endereço http://localhost:8080/person/. Ao acessar esse endereço deverá retornar a lista de todos os usuários já incluídos na base.

### Operações disponíveis ###

* Consulta

    Retorna todos os cadastrados na base. Ex:

    `curl -X GET http://localhost:8080/person/`

    Podemos consultar um usuário pelo ID. Ex:

    `curl -X GET http://localhost:8080/person/ID_USUARIO`

    ou no navegador

    `http://localhost:8080/person/ID_USUARIO` 

    Caso adicione o parametro '?limit', retornará a quantidade de usuários de
    acordo com valor informado.

    `curl -X GET http://localhost:8080/person/?limit=3`

- - - -

+ Inserção

    Insere o usuário com id informado. Ex:

    `curl ­-X POST ­-F facebookId=ID_USUARIO http://localhost:8080/person/`

- - - -

- Exclusão

    Exclui o usuário conforme Id informado. Ex: 

    `curl -X GET http://localhost:8080/person/ID_USUARIO`

### Testes ###
Para executar o teste das operações execute o script run_test.py, localizado na raiz do projeto, com o comando `python -m unittest -v run_test`