#!-*- coding: utf8 -*-
import json
import requests
import sqlite3
import logging

from sqlite3 import IntegrityError
from bottle import Bottle, run
from bottle import get, post, delete, request, response, route
from bottle import static_file
from bottle import error


class FB_Rest:

    def __init__(self, host, port):
        self._host = host
        self._port = port
        self._app = Bottle()
        self._route()

        logging.basicConfig(filename='calls.log', level=logging.DEBUG,
                            format='%(asctime)s %(message)s')

    def _route(self):
        self._app.delete('/person/:user_id', callback=self._delete_user_by_id)
        self._app.get('/person/:user_id', callback=self._list)
        self._app.get('/person/', callback=self._list)
        self._app.post('/person/', callback=self._insert)

    def start(self):
        self._app.run(host=self._host, port=self._port)

    def shutdown(self):
        self._app.reset()

    def _delete_user_by_id(self, user_id=None):

        conn = sqlite3.connect('users_data.db')
        conn_instance = conn.cursor()
        if user_id:
            try:
                id_usernum = int(user_id)

                query_str = 'delete from users where id = ' + user_id
                conn_instance.execute(query_str)
                conn.commit()
                response.status = 204
                logging.debug('Excĺuído usuário com id:' + str(user_id))
                return {'HTTP': str(response.status), 'message': 'excluido\
                usuário com id' + str(user_id)}

            except ValueError as err:
                logging.debug('Obrigatório Id como número inteiro:' + str(err))
                return 'É necessário que o Id informado seja número inteiro.\
                 %s' % (err)

        else:
            return {'message': 'É necessário informar o id válido do usuário.'}

    def _insert(self, user_id=None):
        conn = sqlite3.connect('users_data.db')
        conn_instance = conn.cursor()

        if 'facebookId' in request.POST.dict:
            user_id = request.POST.dict['facebookId'][0]
            user_data = requests.get('https://graph.facebook.com/' +
                                     user_id)
            user_json = user_data.json()
            if 'id' not in user_json:
                logging.debug('Erro ao tentar incluir usuário com id invalido')
                return user_json

            def __is_key(key, json_obj):
                return key in json_obj

            validate_keys = {
                'id': __is_key('id', user_json),
                'username': __is_key('username', user_json),
                'name': __is_key('name', user_json),
                'gender': __is_key('gender', user_json)
            }

            for k, v in validate_keys.iteritems():
                if not v:
                    user_json[k] = u'None'

            query_str = 'insert into users (id, username, name, gender)' \
                        ' values ("' + user_json["id"] + '","' + \
                        user_json["username"].encode('utf8') + '","' + \
                        user_json["name"] + '","' + \
                        user_json["gender"].encode('utf8') + '")'
            query_str.encode('ascii', 'ignore').encode('utf8')

            try:
                conn_instance.execute(query_str)
                conn.commit()
                response.status = 201

                logging.debug('Usuário ' + user_json["id"].encode('utf8') +
                              ' salvo.')

                return {'HTTP ': str(response.status_code), 'message':
                        ' usuário ' + user_json["username"].encode('utf8')
                        + ' com id ' + user_json["id"].encode('utf8')
                        + ' salvo com sucesso.'}

            except IntegrityError as err:
                msg_str = err
                logging.debug('Erro ao tentar incluir usuário \
                    já existente. Exception:' + str(msg_str))
                return {'message': str(msg_str) + '/ usuario existente'}

    def _list(self, user_id=None):

        conn = sqlite3.connect('users_data.db')
        conn_instance = conn.cursor()

        result = {}
        result['users'] = {}

        if user_id:

            conn_instance.execute('select * from users where id = '
                                  + user_id)
            logging.debug('Consulta de usuário com id:' + user_id)
        elif 'limit' in request.GET.dict:
            count = conn_instance.execute('select count( * ) from users')
            if count:
                result['total_users'] = count.fetchone()[0]

            limit = request.GET.get('limit')
            conn_instance.execute('select * from users limit ' + limit)

            logging.debug('Consulta lista de usuário(s) no limite de '
                          + limit)
        else:
            conn_instance.execute('select * from users')
            logging.debug('Retornando todos os usuários')

        for k, item in enumerate(conn_instance.fetchall()):
            result['users'][k] = {}
            result['users'][k].update(
                (conn_instance.description[k][0], v)
                for k, v in enumerate(item)
            )

        return json.dumps(result)

if __name__ == '__main__':
    app = FB_Rest(host='localhost', port=8080)
    app.start()
