#!-*- coding: utf8 -*-
import unittest
import wsgiref.util

from bottle import *
from src.fb_rest import FB_Rest


class FbApiTest(unittest.TestCase):

    def setUp(self):
        '''in setUp()'''
        self._api_test = FB_Rest('localhost', '8080')

    def tearDown(self):
        '''in tearDown()'''
        pass

    def test_instance_api(self):
        '''Cria instancia da api?'''
        self.assertIsInstance(self._api_test, FB_Rest)

    def test_list(self):
        '''Retorna lista populada?'''
        self.assertIsNotNone(self._api_test._list())

    def test_list_limit(self):
        '''Retorna lista populada com parametro limitador?'''
        request = LocalRequest()
        request.GET = FormsDict()
        request.GET.update({'limit': '2'})
        self.assertIsNotNone(self._api_test._list())

    def test_insert_user(self):
        '''Usuário persistido com sucesso?'''
        sq = tob('facebookId=100003054553047')
        e = {}
        wsgiref.util.setup_testing_defaults(e)
        e['wsgi.input'].write(sq)
        e['wsgi.input'].seek(0)
        e['CONTENT_LENGTH'] = str(len(sq))
        e['REQUEST_METHOD'] = "POST"
        request = LocalRequest(e)
        op_message = self._api_test._insert()['message']
        if 'UNIQUE' in op_message:
            self.assertIn('UNIQUE', op_message)
            print op_message
        elif 'salvo com sucesso' in op_message:
            self.assertIn('salvo com sucesso', op_message)
            print op_message

    def test_delete_user_by_id(self):
        '''Usuario com id valido excluido?'''
        user_id = '100003054553047'
        op_message = self._api_test._delete_user_by_id(user_id)['message']
        print op_message


if __name__ == '__main__':
    unittest.main()
